package com.tc.itfarm.service;

import com.tc.itfarm.model.Favorite;

import java.util.List;

/**
 * Created by Administrator on 2016/9/5.
 */
public interface FavoriteService extends BaseService<Favorite> {

    /**
     * 根据用户查询收藏的文章id集合
     * @param userId
     * @return
     */
    List<Integer> selectArticleIds(Integer userId);

}
