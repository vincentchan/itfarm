package com.tc.itfarm.admin.vo;

import com.tc.itfarm.model.User;

import java.io.Serializable;

/**
 * Created by wangdongdong on 2016/8/31.
 */
public class UserVO extends User implements Serializable{

    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
