package com.tc.itfarm.api.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncoderUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncoderUtil.class);

	public static String getBase64(String str) {
		if (StringUtils.isNotBlank(str)) {
		}
		return null;
	}

	/**
	 * MD5加密字符串
	 * @param str
	 * @return
	 */
	public static String md5(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] byteArray = str.getBytes();
			md.update(byteArray);

			byte[] result = md.digest();
			return byteArrayToHex(result);

		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e.getMessage());
			return null;
		}

	}

	private static String byteArrayToHex(byte[] byteArray) {

		// 首先初始化一个字符数组，用来存放每个16进制字符
		char[] hexDigits = {'0','1','2','3','4','5','6','7','8','9', 'A','B','C','D','E','F' };
		// new一个字符数组，这个就是用来组成结果字符串的（解释一下：一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方））
		char[] resultCharArray =new char[byteArray.length * 2];
		// 遍历字节数组，通过位运算（位运算效率高），转换成字符放到字符数组中去
		int index = 0;
		for (byte b : byteArray) {
			resultCharArray[index++] = hexDigits[b>>> 4 & 0xf];
			resultCharArray[index++] = hexDigits[b& 0xf];
		}
		// 字符数组组合成字符串返回
		return new String(resultCharArray);
	}

	public static String toLikeQueryStr(String column) {
		return "%" + column + "%";
	}
}
