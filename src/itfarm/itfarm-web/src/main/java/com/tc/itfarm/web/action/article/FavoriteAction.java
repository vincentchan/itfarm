package com.tc.itfarm.web.action.article;

import com.tc.itfarm.api.common.JsonMessage;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.Favorite;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.ArticleService;
import com.tc.itfarm.service.FavoriteService;
import com.tc.itfarm.web.biz.LoginBiz;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2016/9/5.
 */
@Controller
@RequestMapping("/favorite")
public class FavoriteAction {

    @Resource
    private FavoriteService favoriteService;
    @Resource
    private ArticleService articleService;
    @Resource
    private LoginBiz loginBiz;

    @RequestMapping("/addFavorite")
    @ResponseBody
    public JsonMessage addFavorite(@RequestParam(value = "id", required = true) Integer articleId) {
        User user = loginBiz.getCurUser();
        if (user == null) {
            return new JsonMessage(JsonMessage.STATUS_FAIL, "您还未登录，请先登录！");
        }
        Favorite favorite = new Favorite();
        Article article = articleService.select(articleId);
        favorite.setArticleId(articleId);
        favorite.setArticleTitle(article.getTitle());
        favorite.setUserId(user.getRecordId());
        favorite.setCreateTime(DateUtils.now());
        Integer result = favoriteService.insert(favorite);
        return JsonMessage.toResult(result, "收藏成功", "收藏失败");
    }

}
