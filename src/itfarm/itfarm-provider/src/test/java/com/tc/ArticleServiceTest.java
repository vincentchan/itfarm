package com.tc;

import com.google.common.collect.Lists;
import com.tc.itfarm.service.ArticleService;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2016/9/4.
 */
public class ArticleServiceTest extends BaseTest {
    @Resource
    private ArticleService articleService;

    @Test
    public void testChangeCategory() {
        List<Integer> list = Lists.newArrayList();
        list.add(2);
        list.add(3);
        articleService.modifyToCategory(3, list);
    }
}
